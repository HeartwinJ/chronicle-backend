const router = require("express").Router();
const pool = require("../utils/dbConnection");

router.get("/", async (req, res) => {
  console.info("GET /api/posts/");
  try {
    const result = await pool.query(`SELECT * FROM posts ORDER BY timestamp DESC;`);
    res.json({
      success: true,
      count: result.rows.length,
      posts: result.rows,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      success: false,
      msg: error,
    });
  }
});

router.get("/:post_id", async (req, res) => {
  console.info(`GET /api/posts/${req.params.post_id}`);
  try {
    const result = await pool.query(`SELECT * FROM posts WHERE id = $1;`, [
      req.params.post_id,
    ]);
    res.json({
      success: true,
      post: result.rows[0],
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      success: false,
      msg: error,
    });
  }
});

router.put("/:post_id", async (req, res) => {
  console.info(`PUT /api/posts/${req.params.post_id}`);
  try {
    await pool.query(
      `UPDATE posts SET title = $1, content = $2, timestamp = $3 WHERE id = $4;`,
      [req.body.title, req.body.content, req.body.timestamp, req.params.post_id]
    );
    res.json({
      success: true,
      msg: `Post with ID ${req.params.post_id} updated`,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      success: false,
      msg: error,
    });
  }
});

router.delete("/:post_id", async (req, res) => {
  console.info(`DELETE /api/posts/${req.params.post_id}`);
  try {
    await pool.query(`DELETE FROM posts WHERE id = $1;`, [req.params.post_id]);
    res.json({
      success: true,
      msg: `Post with ID ${req.params.post_id} deleted`,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      success: false,
      msg: error,
    });
  }
});

router.post("/", async (req, res) => {
  console.info("POST /api/posts/");
  try {
    const result = await pool.query(
      `INSERT INTO posts (
        title, content, timestamp
      ) VALUES ( $1, $2, $3 );`,
      [req.body.title, req.body.content, req.body.timestamp]
    );
    res.json({
      success: true,
      msg: `Post with ID ${req.params.post_id} added`,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      success: false,
      msg: error,
    });
  }
});

module.exports = router;
