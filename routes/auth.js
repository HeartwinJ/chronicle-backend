const router = require("express").Router();
const jwt = require("jsonwebtoken");
const verifyToken = require("../utils/verifyToken");

router.post("/login", (req, res) => {
  console.info('POST /api/auth/login');
  if (req.body.password != process.env.PORTAL_PASSWORD) {
    return res.status(400).send({
      success: false,
      msg: "Password Incorrect!",
    });
  }

  const token = jwt.sign({}, process.env.TOKEN_SECRET, { expiresIn: 600 });
  res.json({ success: true, msg: "Logged In!", token: token });
});

router.post("/refresh", verifyToken, (req, res) => {
  console.info('POST /api/auth/refresh');
  const token = jwt.sign({}, process.env.TOKEN_SECRET, { expiresIn: 600 });
  res.json({ success: true, msg: "Logged In!", token: token });
});

module.exports = router;
