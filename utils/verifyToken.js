const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
  const token = req.header("auth-token");
  if (!token) {
    res.status(401).json({
      success: false,
      message: "Access Denied!",
    });
    return;
  }

  try {
    jwt.verify(token, process.env.TOKEN_SECRET);
    next();
  } catch (err) {
    res.status(400).json({
      status: false,
      message: "Invalid Token!",
    });
  }
};
