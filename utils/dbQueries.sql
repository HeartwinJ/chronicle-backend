CREATE TABLE posts(
	id SERIAL PRIMARY KEY,
	title VARCHAR(255),
	content TEXT,
	timestamp BIGINT
);

SELECT * FROM posts WHERE;

SELECT * FROM posts WHERE id = 2;

UPDATE posts SET title = 'Post 2', content = '# Test Post 2', timestamp = 1231231231232 WHERE id = 2;

DELETE FROM posts WHERE id = 2;

INSERT INTO posts (
	title,
	content,
	timestamp
) VALUES (
	'Post 1',
	'# Test Post 1',
	1231231231231
);
