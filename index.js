const express = require("express");
const app = express();
const cors = require("cors");
const dotenv = require("dotenv");

dotenv.config();

const verifyToken = require("./utils/verifyToken");

const authRoutes = require("./routes/auth");
const postsRoutes = require("./routes/posts");

const PORT = process.env.PORT || 3001;

const router = express.Router();

app.use(express.json());
app.use(cors());

app.use("/api", router);

router.get("/", (req, res) => {
  res.json({
    success: true,
    msg: "Fetched Available APIs",
    apis: [
      "POST /api/auth/login",
      "POST /api/auth/refresh",
      "GET /api/posts",
      "POST /api/posts",
      "GET /api/posts/{postID}",
      "PUT /api/posts/{postID}",
      "DELETE /api/posts/{postID}",
    ],
  });
});

router.use("/auth", authRoutes);
router.use("/posts", verifyToken, postsRoutes);

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}`);
});
